import unittest
from Check_Phone import check_phone
import re

class TestPhone(unittest.TestCase):
    def setUp(self):
        self.test_check_phon = check_phone()

    def test_check_phone(self):
        self.assertTrue(self.test_check_phon.check_num('+79160000000'))
        self.assertTrue(self.test_check_phon.check_num('9160000000'))
        self.assertTrue(self.test_check_phon.check_num('8-916-000-00-00'))
        self.assertTrue(self.test_check_phon.check_num('916-000-00-00'))
        self.assertTrue(self.test_check_phon.check_num('(916)0000000'))
        self.assertFalse(self.test_check_phon.check_num('4521'))

